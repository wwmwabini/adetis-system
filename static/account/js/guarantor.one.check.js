<script type="text/javascript">
       $(document).ready(function() {
           $("#guarantor_1").on('input', function(e) {
                $('#msg').hide();
                $('#loading').show();
                if ($('#guarantor_1').val() == null || $('#guarantor_1').val() == "") {
                $('#msg').show();
                $("#msg").html("Guarantor is required!").css("color", "red");
                 } else {
                 $.ajax({
                  type: "POST",
                  url: "/guarantorone",
                  data: $('#applyloan').serialize(),
                  dataType: "html",
                  cache: false,
                  success: function(msg) {
                   $('#msg').show();
                   $('#loading').hide();
                   $("#msg").html(msg);
                  },
                  error: function(jqXHR, textStatus, errorThrown) {
                   $('#msg').show();
                   $('#loading').hide();
                   $("#msg").html(textStatus + " " + errorThrown);
                    }
                 });
               }
             });
           });
</script>