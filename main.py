from flask import Flask, render_template, request, flash, redirect, url_for, session, jsonify, make_response, send_file, \
    send_from_directory
from passlib.hash import sha256_crypt
import pymysql, re, os, pdfkit
from flask_mail import Mail, Message
from config import *
from datetime import datetime, date, time
from functools import wraps, update_wrapper
from flask_uploads import UploadSet, configure_uploads, patch_request_class, IMAGES

app = Flask(__name__)
app.secret_key = APP_SECRET_KEY

dbconnection = pymysql.connect(host=DB_HOST, user=DB_USERNAME, password=DB_PASSWORD, db=DB_NAME)

app.config['MAIL_SERVER'] = MAIL_SERVER
app.config['MAIL_PORT'] = MAIL_PORT
app.config['MAIL_USERNAME'] = MAIL_USERNAME
app.config['MAIL_PASSWORD'] = MAIL_PASSWORD
app.config['MAIL_USE_TLS'] = True
app.config['MAIL_USE_SSL'] = False
mail = Mail(app)


photos = UploadSet('photos', IMAGES)
app.config['UPLOADED_PHOTOS_DEST'] = 'static/images/profiles'
configure_uploads(app, photos)
patch_request_class(app, 2 * 180 * 180)


def nocache(view):
    @wraps(view)
    def no_cache(*args, **kwargs):
        response = make_response(view(*args, **kwargs))
        response.headers['Last-Modified'] = datetime.now()
        response.headers['Cache-Control'] = 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0'
        response.headers['Pragma'] = 'no-cache'
        response.headers['Expires'] = '-1'
        return response

    return update_wrapper(no_cache, view)



@app.route("/register/", methods=['GET', 'POST'])
def registration():
    try:
        if request.method == 'POST':
            firstname = request.form['user_fname']
            middlename = request.form['user_mname']
            lastname = request.form['user_lname']
            phonenumber = request.form['user_phone']
            emailaddress = request.form['user_email']
            password1 = request.form['user_pass1']
            password2 = request.form['user_pass2']
            physicaladdress = request.form['physical_address']
            gender = request.form['user_gender']

            cur = dbconnection.cursor()
            cur.execute('SELECT * FROM members WHERE email = %s', emailaddress)
            member = cur.fetchone()
            cur.close()
            if member:
                flash('A user with the email address exists. Please try again!', 'danger')
                return redirect(url_for('registration'))
            elif password1 != password2:
                flash('The passwords entered must match. Please try again', 'danger')
                return redirect(url_for('registration'))
            elif len(password1) < 7:
                flash('Password must be at least 7 characters long.', 'danger')
                return redirect(url_for('registration'))
            elif not re.search('[a-z]', password1):
                flash('Password must include at least one small letter.', 'danger')
                return redirect(url_for('registration'))
            elif not re.search('[A-Z]', password1):
                flash('Password must include at least one capital letter.', 'danger')
                return redirect(url_for('registration'))
            elif not re.search('[0-9]', password1):
                flash('Password must include at least one digit.', 'danger')
                return redirect(url_for('registration'))
            else:
                otp = os.urandom(12).hex()
                confirmationurl = PROTOCOL + URL + DOC_ROOT + 'completeregistration/'+'?regkey=' +'&otp=' + otp +'&mail=' + emailaddress
                db_password = sha256_crypt.hash(password1)

                cur = dbconnection.cursor()
                cur.execute(
                    'INSERT INTO members(firstname, middlename, lastname, email, phone, address, password, gender, '
                    'registration_token) '
                    'VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s)',
                    (firstname, middlename, lastname, emailaddress, phonenumber, physicaladdress, db_password, gender, otp))
                cur.execute('UPDATE members SET expiry_registration_token = now() + INTERVAL 1 HOUR WHERE email = %s',
                            emailaddress)
                dbconnection.commit()
                cur.close()

                msg = Message('OTP', sender=DEFAULT_SENDER, recipients=[emailaddress])
                msg.html = render_template('/account/otpconfirmation.html', firstname=firstname, otp=otp,
                                           confirmationurl=confirmationurl)
                mail.send(msg)

                flash('Success! You have successfully created an account. Check email for further steps.', 'success')
                return redirect(url_for('registration'))

    except Exception as e:
        return render_template('500.html', error=e)

    return render_template("/account/register.html")


@app.route("/completeregistration/", methods=['GET', 'POST'])
def completereg():
    otp = request.args.get('otp')
    emailaddress = request.args.get('mail')

    cur = dbconnection.cursor()
    cur.execute("SELECT * FROM members WHERE registration_token = %s", otp)
    member = cur.fetchone()

    if member:
        cur = dbconnection.cursor()
        cur.execute("UPDATE members SET status = %s, expiry_registration_token=now() WHERE registration_token = %s",
                    ('active', otp))
        dbconnection.commit()

        msg = Message('Registration Confirmation', sender=DEFAULT_SENDER, recipients=[emailaddress])
        msg.html = render_template('/account/registrationcompletion.html', ORG_NAME=ORG_NAME)
        mail.send(msg)

        flash('Success! Registration compeleted, you can now login.', 'success')
        return redirect(url_for('login'))

    else:
        flash("The OTP is invalid. Please access portal and resend the validation email.", "danger")
        return redirect(url_for('login'))
    #return render_template('/account/registrationcompletion.html')




@app.route("/login/", methods=['GET', 'POST'])
def login():
    #try:
    if request.method == 'POST':
        loginemail = request.form['login_email']
        loginpassword = request.form['login_password']

        cur = dbconnection.cursor()
        cur.execute('SELECT membernumber,firstname,lastname,password FROM members WHERE email = %s', (loginemail))
        member = cur.fetchone()
        (membernumber, firstname, lastname, password) = member #unpack tuple

        #sha256_crypt.verify(txt, hash)
        isPasswordCorrect = sha256_crypt.verify(loginpassword, password)

        cur.close()

        if isPasswordCorrect == True:
            #session['loggedin'] = True
            session['loginemail'] = loginemail
            session['membernumber'] = membernumber
            session['firstname'] = firstname
            session['lastname'] = lastname

            flash('Welcome back. You have successfully logged in.', 'success')
            return redirect(url_for('dashboard'))
        else:
            flash('Invalid login. Please try again.', 'danger')
            return redirect(url_for('login'))

    #except Exception as e:
    #    return render_template('500.html', error=e)

    return render_template("/account/login.html")


@app.route("/pwreset/", methods=['GET', 'POST'])
def forgotpassword():
    try:
        if request.method == 'POST':
            loginemail = request.form['login_email']

            cur = dbconnection.cursor()
            cur.execute('SELECT * FROM members WHERE email = %s', loginemail)
            member = cur.fetchone()
            cur.close()

            if member:
                flash('Please check your inbox (or spam/junk folder) for password reset instructions we have sent.',
                      'info')
                resetkey = os.urandom(12).hex()
                now = datetime.now()
                timenow = now.strftime("%H:%M:%S")
                day = str(date.today())
                finalkey = '?rusetkey=&time=' + timenow + '-&day=' + day + '-&key=' + resetkey + '&mail=' + loginemail
                reseturl = PROTOCOL + URL + DOC_ROOT + 'keycheck/' + finalkey

                cur = dbconnection.cursor()
                cur.execute('UPDATE members SET reset_token = %s WHERE email = %s', (resetkey, loginemail))
                cur.execute('UPDATE members SET expiry_reset_token=now() + INTERVAL 30 MINUTE WHERE email = %s',
                            loginemail)
                dbconnection.commit()
                cur.close()

                msg = Message('Password Reset Instructions', sender=DEFAULT_SENDER, recipients=[loginemail])
                msg.html = render_template('/account/passwordreset.html', loginemail=loginemail, reseturl=reseturl,
                                           ORG_NAME=ORG_NAME)
                mail.send(msg)
                return redirect(url_for('forgotpassword'))
            else:
                flash('The email address does not exists in our system.', 'warning')
                return redirect(url_for('forgotpassword'))

    except Exception as e:
        return render_template('500.html', error=e)

    return render_template("/account/forgot-password.html")




@app.route("/keycheck/", methods=['GET', 'POST'])
def keycheck():
    resetkey = request.args.get('key')
    loginemail = request.args.get('mail')

    cur = dbconnection.cursor()
    cur.execute('SELECT expiry_reset_token FROM members WHERE reset_token = %s', resetkey)
    member = cur.fetchone()
    cur.close()

    if not member:
        flash('The key is invalid. Please repeat the reset procedure.', 'danger')
        return redirect(url_for('login'))
    else:
        session['loginemail'] = loginemail

        if request.method == 'POST':
            password1 = request.form['user_pass1']
            password2 = request.form['user_pass2']

            if password1 != password2:
                flash('The passwords entered must match. Please try again', 'danger')
                return redirect(request.referrer)
            elif len(password1) < 7:
                flash('Password must be at least 7 characters long.', 'danger')
                return redirect(request.referrer)
            elif not re.search('[a-z]', password1):
                flash('Password must include at least one small letter.', 'danger')
                return redirect(request.referrer)
            elif not re.search('[A-Z]', password1):
                flash('Password must include at least one capital letter.', 'danger')
                return redirect(request.referrer)
            elif not re.search('[0-9]', password1):
                flash('Password must include at least one digit.', 'danger')
                return redirect(url_for('changepass'))
            else:
                db_password = sha256_crypt.hash(password1)
                cur = dbconnection.cursor()
                cur.execute('UPDATE members SET password = %s WHERE email = %s', (db_password, loginemail))
                dbconnection.commit()
                cur.close()
                flash('Your password has been successfully updated.', 'success')
                return redirect(url_for('login'))
    return render_template('/account/newpassword.html')




@app.route("/dashboard/")
@nocache
def dashboard():
    if 'loginemail' in session:

        firstname = session.get('firstname')
        lastname = session.get('lastname')

        return render_template("/account/index.html", firstname=firstname, lastname=lastname)
    else:
        flash("You're not logged in. Please login.", 'info')
        return redirect(url_for('login'))
    #return redirect(url_for('login'))



@app.route("/apply-loan/", methods=["GET", "POST"])
@nocache
def applyloan():
    if 'loginemail' in session:

        useremail = session.get('loginemail')
        membernumber = session.get('membernumber')
        firstname = session.get('firstname')
        lastname = session.get('lastname')

        if request.method == 'POST':
            loantype = request.form['loan_type']
            grosspay = request.form['gross_salary']
            totaldeductions = request.form['total_deductions']
            amountrequested = request.form['loan_amount']
            repaymentperiod = request.form['repayment_period']
            disbursementmethod = request.form['disbursement_method']
            guarantor1 = request.form['guarantor_1']
            guarantor2 = request.form['guarantor_2']

            #update db with details
            cur = dbconnection.cursor()
            cur.execute('INSERT INTO loans (membernumber, loantype, grosspay, totaldeductions, amount_requested, repayment_period, '
                        'disbursement_mode, guarantor1, guarantor2) '
            'VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s)',
            (membernumber, loantype, grosspay, totaldeductions, amountrequested, repaymentperiod, disbursementmethod,
             guarantor1, guarantor2))
            dbconnection.commit()
            cur.execute('SELECT id, membernumber FROM loans WHERE membernumber = %s', membernumber)
            memberloan = cur.fetchone()
            cur.close

            (id, *others) = memberloan

            #send details including a pdf of loan details to user
            msg = Message('Loan Application Confirmation', sender=DEFAULT_SENDER, recipients=[useremail])
            msg.html = render_template('/account/loanappconfirmation.html', loanid=id, firstname=firstname, lastname=lastname,
                                       amountrequested=amountrequested, repaymentperiod=repaymentperiod,
                                       disbursementmethod=disbursementmethod, ORG_NAME=ORG_NAME
                                       )
            html = render_template('/account/loanappreceipt.html', loanid=id, firstname=firstname, lastname=lastname,
                                   amountrequested=amountrequested, repaymentperiod=repaymentperiod,
                                   disbursementmethod=disbursementmethod, ORG_NAME=ORG_NAME
                                   )
            workingdir = os.path.abspath(os.getcwd())
            options = {
                'page-size': 'A4',
                'margin-top': '0.75in',
                'margin-right': '0.75in',
                'margin-bottom': '0.75in',
                'margin-left': '0.75in',
            }
            inputfile = workingdir+'/templates/account/loanappreceipt.html'
            outputfile = 'L-'+ str(id) +'.pdf'
            pdfkit.from_file(inputfile, outputfile, options=options)
            with open(outputfile, 'rb') as fh:
                msg.attach(filename=outputfile, disposition="attachment", content_type="application/pdf",
                           data=fh.read())
            mail.send(msg)

            #send guarantors a notice in email and dashboard to approve request

            #send admin a notice of loan application on email and dashboard

            flash('Success! Loan application has been submitted for approval. Allow upto 48hrs. ', 'success')
            return redirect(url_for('dashboard'))
    else:
        flash("You're not logged in. Please login.", 'info')
        return redirect(url_for('login'))

    return render_template("/account/applyloans.html")



@app.route("/loans-issued/")
@nocache
def loansissued():
    if 'loginemail' in session:
        return render_template("/account/loansissued.html")
    else:
        flash("You're not logged in. Please login.", 'info')
        return redirect(url_for('login'))
    return redirect(url_for('login'))



@app.route("/loans-guaranteed/")
@nocache
def loansguaranteed():
    if 'loginemail' in session:
        return render_template("/account/loansguaranteed.html")
    else:
        flash("You're not logged in. Please login.", 'info')
        return redirect(url_for('login'))
    return redirect(url_for('login'))



@app.route("/guaranteerequests/")
@nocache
def guaranteerequests():
    if 'loginemail' in session:
        return render_template("/account/guaranteerequests.html")
    else:
        flash("You're not logged in. Please login.", 'info')
        return redirect(url_for('login'))
    return redirect(url_for('login'))



@app.route("/loans-calc/")
@nocache
def loancalc():
    if 'loginemail' in session:
        return render_template("/account/loancalculator.html")
    else:
        flash("You're not logged in. Please login.", 'info')
        return redirect(url_for('login'))
    return redirect(url_for('login'))




@app.route('/guarantorone', methods=['POST'])
@nocache
def guarantor_check():
    if 'loginemail' in session:
        try:
            guarantor1 = request.form['guarantor_1']

            # validate the received values
            if guarantor1 and request.method == 'POST':

                cur = dbconnection.cursor()
                cur.execute("SELECT * FROM members WHERE id = %s", guarantor1)
                record = cur.fetchone()
                #cur.close

                if not record:
                    resp = jsonify('<span style="\'color:red;\'">Invalid Guarantor</span>')
                    resp.status_code = 200
                    return resp
                else:
                    resp = jsonify('<span style="\'color:green;\'">Ok!</span>')
                    resp.status_code = 200
                    return resp
            else:
                resp = jsonify('<span style="\'color:red;\'">Guarantor is required field.</span>')
                resp.status_code = 200
                return resp
        except Exception as e:
            print(e)
        #finally:
        #    cur.close()
        #    dbconnection.close()
    else:
        flash("You're not logged in. Please login.", 'info')
        return redirect(url_for('login'))

@app.route('/guarantortwo', methods=['POST'])
@nocache
def guarantor2_check():
    if 'loginemail' in session:
        try:
            guarantor1 = request.form['guarantor_1']
            guarantor2 = request.form['guarantor_2']

            if guarantor1 != guarantor2:
                # validate the received values
                if guarantor2 and request.method == 'POST':

                    cur = dbconnection.cursor()
                    cur.execute("SELECT * FROM members WHERE id = %s", guarantor2)
                    record = cur.fetchone()
                    cur.close

                    if not record:
                        resp = jsonify('<span style="\'color:red;\'">Invalid Guarantor</span>')
                        resp.status_code = 200
                        return resp
                    else:
                        resp = jsonify('<span style="\'color:green;\'">Ok!</span>')
                        resp.status_code = 200
                        return resp
                else:
                    resp = jsonify('<span style="\'color:red;\'">Guarantor is required field.</span>')
                    resp.status_code = 200
                    return resp
            else:
                resp = jsonify('<span style="\'color:red;\'">2nd guarantor must be different from 1st.</span>')
                resp.status_code = 200
                return resp

        except Exception as e:
            print(e)
        #finally:
        #    cur.close()
        #    dbconnection.close()
    else:
        flash("You're not logged in. Please login.", 'info')
        return redirect(url_for('login'))


@app.route('/profile/', methods=['GET', 'POST'])
@nocache
def viewprofile():
    if 'loginemail' in session:

        currentemail = session.get('loginemail')
        cur = dbconnection.cursor()
        cur.execute('SELECT membernumber, firstname, middlename, lastname, email, phone, identification_no, photo_location, address, '
                    'password FROM members WHERE email = %s', currentemail)
        member = cur.fetchone()

        (membernumber, firstname, middlename, lastname, email, phone, identification_no, photo_location, address, password) = member

    return render_template('/account/profile.html', membernumber=membernumber, firstname=firstname,
                           middlename=middlename, lastname=lastname, email=email, phone=phone,
                           identification_no=identification_no, photo_location=photo_location,
                           address=address, password=password)



@app.route('/profile/changepass/', methods=['GET', 'POST'])
@nocache
def updatepassword():
    if 'loginemail' in session:

        currentemail = session.get('loginemail')

        cur = dbconnection.cursor()
        cur.execute('SELECT password FROM members WHERE email = %s', currentemail)
        member = cur.fetchone()

        (password, *others) = member

        # sha256_crypt.verify(txt, hash)
        if request.method == 'POST':
            current_pass = request.form['current_pass']
            new_password1 = request.form['new_password1']
            new_password2 = request.form['new_password2']

            isPasswordCorrect = sha256_crypt.verify(current_pass, password)


            if isPasswordCorrect:
                if new_password1 != new_password2:
                    flash('The passwords entered must match. Please try again', 'danger')
                    return redirect(url_for('profile'))
                elif len(new_password1) < 7:
                    flash('Password must be at least 7 characters long.', 'danger')
                    return redirect(url_for('profile'))
                elif not re.search('[a-z]', new_password1):
                    flash('Password must include at least one small letter.', 'danger')
                    return redirect(url_for('profile'))
                elif not re.search('[A-Z]', new_password1):
                    flash('Password must include at least one capital letter.', 'danger')
                    return redirect(url_for('profile'))
                elif not re.search('[0-9]', new_password1):
                    flash('Password must include at least one digit.', 'danger')
                    return redirect(url_for('profile'))
                else:
                    db_password = sha256_crypt.hash(new_password1)
                    cur = dbconnection.cursor()
                    cur.execute('UPDATE members SET password = %s WHERE email = %s', (db_password, currentemail))
                    dbconnection.commit()
                    cur.close()
                    flash('Your password has been successfully updated.', 'success')
                    return redirect(url_for('updatepassword'))
            return redirect(url_for('dashboard'))
        return render_template('/account/profile.html')



@app.route('/profile/editdetails/', methods=['GET', 'POST'])
@nocache
def updateprofile():
    if 'loginemail' in session:

        currentemail = session.get('loginemail')

        if request.method == 'POST':
            firstname = request.form['fname']
            middlename = request.form['mname']
            lastname = request.form['lname']
            email = request.form['email_address']
            phone = request.form['phone_no']
            address = request.form['physical_address']

            cur = dbconnection.cursor()
            cur.execute('UPDATE members SET firstname=%s, middlename=%s, lastname=%s, '
                        'email=%s, phone=%s, address=%s WHERE email = %s',
                        (firstname, middlename, lastname, email, phone, address, currentemail))
            dbconnection.commit()
            cur.close()
            flash('Your profile details have been successfully updated.', 'success')
            return redirect(url_for('updateprofile'))

    return render_template('/account/profile.html')




@app.route('/uploads/pp/', methods = ['GET', 'POST'])
@nocache
def upload_photo():
    if 'loginemail' in session:

        currentemail = session.get('loginemail')

        if request.method == 'POST' and 'photo' in request.files:

            cur = dbconnection.cursor()
            cur.execute('SELECT id FROM members WHERE email = %s', currentemail)
            member = cur.fetchone()

            (id, *others) = member

            profilepic_name = str(id)+'.png'
            profilepic_url = '/static/images/profiles/'+profilepic_name
            workingdir = os.path.abspath(os.getcwd())
            fullprofilepic_url = workingdir + profilepic_url

            if os.path.isfile(fullprofilepic_url) == True:
                os.remove(fullprofilepic_url)

            photos.save(request.files['photo'], folder=None, name=profilepic_name)
            flash("Success! Profile photo uploaded successfully.", 'success')

            cur = dbconnection.cursor()
            cur.execute('UPDATE members SET photo_location = %s WHERE email = %s', (profilepic_url, currentemail))
            dbconnection.commit()
            cur.close()

            return redirect(url_for('viewprofile'))



@app.route('/downloads/strategic_plan')
def strategicplan():
    workingdir = os.path.abspath(os.getcwd())
    filepath = workingdir + '/static/files/'
    return send_from_directory(filepath, 'linuxsto.pdf')


@app.route('/downloads/member_forms/')
def memberforms():
    workingdir = os.path.abspath(os.getcwd())
    filepath = workingdir + '/static/files/'
    return send_from_directory(filepath, 'linuxsto.pdf')


@app.route('/links/tos/')
def tos():
    return render_template("/account/tos.html")


@app.route('/links/pp/')
def pp():
    return render_template("/account/privacy_policy.html")


@app.route('/links/dr/')
def dr():
    return render_template("/account/dispute_resolution.html")




@app.route('/logout/')
@nocache
def logout():
    session.pop('loginemail')
    session.pop('membernumber')
    session.pop('firstname')
    session.pop('lastname')
    return redirect(url_for('login'))


@app.errorhandler(404)
def pagenotfound(e):
    return render_template("404.html")


@app.errorhandler(500)
def servererror(e):
    return render_template("500.html")

@app.errorhandler(413)
def entitytoolarge(e):
    flash('Failed! Please upload an image less than 1MB and 150x150','warning')
    return redirect(url_for('viewprofile'))

if __name__ == "__main__":
    app.run(debug=True, port=5002)